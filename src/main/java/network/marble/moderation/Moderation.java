package network.marble.moderation;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;

import network.marble.messageapi.api.MessageAPI;
import network.marble.messageapi.api.MessageExecutor;
import network.marble.moderation.commands.*;
import network.marble.moderation.permissions.Permissions;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import lombok.Getter;
import network.marble.datastoragemanager.config.Settings;
import network.marble.moderation.commands.debug.AddPermission;
import network.marble.moderation.commands.debug.CreateRank;
import network.marble.moderation.commands.debug.RemovePermission;
import network.marble.moderation.commands.debug.RemoveRank;
import network.marble.moderation.commands.debug.SetInheritance;
import network.marble.moderation.commands.debug.SetRank;
import network.marble.moderation.commands.debug.UnBan;
import network.marble.moderation.config.MainConfig;
import network.marble.moderation.listeners.PlayerListener;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

public class Moderation extends JavaPlugin
{
	@Getter private static String pluginName = "[MRN] Moderation";
	@Getter private static Moderation instance;
	@Getter private static JedisPool pool;
	public static MainConfig config;
	public static final ModerationMessageExecutor messageClass = new ModerationMessageExecutor();

	public void onEnable() {
		instance = this;

		config = retrieveConfig();

		if (config == null){
			createConfig();
			config = retrieveConfig();
		}

		MessageAPI.registerPlugin(getPluginName(), messageClass);

		registerCommands();
		registerListeners();
		setupJedisPool();
	}

	public void onDisable() {
		pool.destroy();
	}

	private void setupJedisPool(){
		try{
			JedisPoolConfig poolcfg = new JedisPoolConfig();
			poolcfg.setMaxTotal(getServer().getMaxPlayers() + 1);//Everyone can disconnect simultaneously
			pool = new JedisPool(new JedisPoolConfig(), config.getRedisHost(), config.getRedisPort());
		}catch(Exception e){
			getLogger().severe("Failed to load at Redis setup.");
			e.printStackTrace();
			setEnabled(false);
		}
	}

	public void registerListeners(){
		getServer().getPluginManager().registerEvents(new PlayerListener(), this);
	}

	public void registerCommands(){
		this.getCommand("setrank").setExecutor(new SetRank());
		this.getCommand("addpermission").setExecutor(new AddPermission());
		this.getCommand("removepermission").setExecutor(new RemovePermission());
		this.getCommand("removerank").setExecutor(new RemoveRank());
		this.getCommand("setinheritance").setExecutor(new SetInheritance());
		this.getCommand("createrank").setExecutor(new CreateRank());
		this.getCommand("ban").setExecutor(new Ban());
		this.getCommand("kick").setExecutor(new Kick());
		this.getCommand("kickhub").setExecutor(new KickHub());
		this.getCommand("mute").setExecutor(new Mute());
		this.getCommand("confirm").setExecutor(new Confirm());
		this.getCommand("cancel").setExecutor(new Cancel());
		this.getCommand("unban").setExecutor(new UnBan());
		this.getCommand("listranks").setExecutor(new RankList());
		this.getCommand("schat").setExecutor(new StaffChat());
	}

	private MainConfig retrieveConfig(){
		try(Reader reader = new FileReader("plugins/MRN-Moderation/config.json")){
			MainConfig container = new Gson().fromJson(reader, MainConfig.class);

			return container;
		} catch (Exception e){
			getLogger().info("Config does not exist... Creating");
		}
		return null;
	}

	private void createConfig(){
		MainConfig builder = new MainConfig(); 
		File directory = new File("plugins/MRN-Moderation");
		
		if (directory.exists() == false){
			getLogger().info("Creating Directory");
			directory.mkdir();
			getLogger().info("Created");
		}
		
		try(Writer writer = new BufferedWriter( new OutputStreamWriter( new FileOutputStream("plugins/MRN-Moderation/config.json" )))){
			Gson file = new GsonBuilder().create();
			file.toJson(builder, writer);
			getLogger().info("Config file has been created");
		} catch (Exception e){
			e.printStackTrace();
			this.setEnabled(false);
		}
	}
}
