package network.marble.moderation.config;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MainConfig {

	private Boolean debug = false;

	private Boolean verbose = false;
	
	private String redisHost = "localhost";
	
	private String redisUser = "";
	
	private String redisPass= "";
	
	private int redisPort = 6379;
}