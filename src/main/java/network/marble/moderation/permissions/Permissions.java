package network.marble.moderation.permissions;

import network.marble.datastoragemanager.api.DataAPI;
import network.marble.datastoragemanager.database.models.NetworkUser;
import network.marble.datastoragemanager.database.models.Rank;
import network.marble.datastoragemanager.database.models.RankPermission;
import network.marble.moderation.Moderation;

import org.bukkit.entity.Player;
import org.bukkit.permissions.Permission;
import org.bukkit.permissions.PermissionAttachment;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class Permissions {
    private static HashMap<UUID,PermissionAttachment> players = new HashMap<UUID, PermissionAttachment>();
    private static HashMap<Rank,List<RankPermission>> permissions = new HashMap<Rank, List<RankPermission>>();
    static JavaPlugin plugin = Moderation.getInstance();

    public static void addPlayerAttachment(Player p){
        PermissionAttachment attachment = registerPermissions(p, p.addAttachment(plugin));

        players.put(p.getUniqueId(), attachment);
        if(Moderation.config.getVerbose()) {
            Moderation.getInstance().getLogger().info("Player stored in hashmap");
        }
    }

    public static void unRegisterAllAttachements(Player p){
        PermissionAttachment attachment = players.get(p.getUniqueId());
        p.removeAttachment(attachment);
    }

    private static PermissionAttachment registerPermissions(Player p, PermissionAttachment attachment){
        if(Moderation.config.getVerbose()) {
            Moderation.getInstance().getLogger().info("registering Permissions");
        }
        NetworkUser u = DataAPI.retrieveUser(p.getUniqueId().toString());

        if(Moderation.config.getVerbose()) {
            Moderation.getInstance().getLogger().info("for user " + u.getName());
        }
        Rank rank = DataAPI.getNetworkRank(u.getRank());

        if(Moderation.config.getVerbose()) {
            Moderation.getInstance().getLogger().info("who is of rank " + rank.getName());
        }

        List<RankPermission> perms = DataAPI.getPermissions(rank);
        if(perms == null){
            Moderation.getInstance().getLogger().info("perms null");
            return attachment;
        }

        if(Moderation.config.getVerbose()) {
            Moderation.getInstance().getLogger().info("Permissions retrieved from database");
        }
        permissions.put(rank, perms);

        if(Moderation.config.getVerbose()) {
            Moderation.getInstance().getLogger().info("Stored permissions");
        }

        for(RankPermission perm : perms){
            if(Moderation.config.getVerbose()) {
                Moderation.getInstance().getLogger().info("Attaching permission " + perm.getNode());
            }

            attachment.setPermission(perm.getNode(), true);

            if(Moderation.config.getVerbose()) {
                Moderation.getInstance().getLogger().info("Attached");
            }
        }

        return attachment;
    }

    private static void unRegisterPermissions(Player p, PermissionAttachment attachment){
        attachment.remove();
    }

    public static void recalculatePermissions(Player p){
        PermissionAttachment attachment = players.get(p.getUniqueId());
        unRegisterPermissions(p, attachment);
        players.remove(p.getUniqueId());
        addPlayerAttachment(p);
    }

}
