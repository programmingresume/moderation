package network.marble.moderation.commands;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import network.marble.datastoragemanager.api.DataAPI;
import network.marble.datastoragemanager.database.DAOManager;
import network.marble.datastoragemanager.database.models.ModerationLog;
import network.marble.datastoragemanager.database.models.NetworkUser;
import network.marble.moderation.Moderation;
import network.marble.moderation.utils.FontFormat;
import redis.clients.jedis.Jedis;

public class Confirm implements CommandExecutor {

	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if(sender instanceof Player){
			NetworkUser user = DataAPI.retrieveUser(((Player) sender).getUniqueId().toString());
			if(sender.hasPermission("moderation.commands.ban")){
			//FIXME: Permissions
				Player pSender = (Player) sender;
				Map<String, String> userData = new HashMap<String, String>();

				try{
					Jedis jedis = Moderation.getPool().getResource();
					if(jedis.exists("moderation:" + pSender.getUniqueId())){
						userData = jedis.hgetAll("moderation:" + pSender.getUniqueId());
						jedis.del("moderation:" + pSender.getUniqueId());
					}
					else{
						sender.sendMessage(FontFormat.translateString("&cYou can only use this command after first using the ban command"));
						return false;
					}
				}catch(Exception e){
					sender.sendMessage(FontFormat.translateString("&cThere was an error whilst communicating with the database. "
							+ "Please try again shortly or create a bug report"));
					return false;
				}

				NetworkUser u = DataAPI.retrieveUser(userData.get("targetUUID"));
				u.getNetworkUserModeration().setBanEndTime(System.currentTimeMillis() + Long.parseLong(userData.get("totalMillis")));
				u.getNetworkUserModeration().setBanned(true);
				DataAPI.saveUser(u);

				String reason = "";
				for (String s : args){
					reason += " " + s;
				}

				Player p = Bukkit.getPlayer(u.getName());

				ModerationLog m = new ModerationLog(pSender.getUniqueId().toString(), 
						p.getUniqueId().toString(), System.currentTimeMillis(), "BAN", reason, System.currentTimeMillis() + Long.parseLong(userData.get("totalMillis")));
				DAOManager.getModerationLogDAO().save(m);

				p.kickPlayer("You have been banned for reason: " + reason);
			} else {
				sender.sendMessage(FontFormat.translateString("&cYou do not have permission to execute this command!"));
				return false;
			}
		}
		else{
			sender.sendMessage(FontFormat.translateString("&cYou must be a player to send this command!"));
		}
		return false;
	}
}
