package network.marble.moderation.commands;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.TextComponent;
import network.marble.datastoragemanager.api.DataAPI;
import network.marble.datastoragemanager.database.DAOManager;
import network.marble.datastoragemanager.database.models.NetworkUser;
import network.marble.datastoragemanager.database.models.Rank;
import network.marble.datastoragemanager.database.models.RankPermission;
import network.marble.messageapi.api.MessageAPI;
import network.marble.moderation.Moderation;
import network.marble.moderation.utils.FontFormat;
import network.marble.serveridentifier.api.IdentifierAPI;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class StaffChat implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            if (sender.hasPermission("moderation.chat.staff")) {
                if (args.length >= 1) {
                    List<Rank> validRanks = new ArrayList<>();
                    List<Rank> ranks = DAOManager.getRankDAO().find().asList();
                    Long serverID = IdentifierAPI.getServerID();

                    for (Rank r : ranks) {
                        List<RankPermission> perms = r.getRankPermission();
                        Moderation.getInstance().getLogger().info(r.getName());
                        for (RankPermission p : perms) {
                            Moderation.getInstance().getLogger().info(p.getNode());
                            if (p.getNode().equals("moderation.chat.staff")) {
                                Moderation.getInstance().getLogger().info(r.getName() + " valid");
                                validRanks.add(r);
                                break;
                            }
                        }
                    }

                    List<NetworkUser> usersToSend = new ArrayList<>();

                    for (Rank r : validRanks) {
                        Moderation.getInstance().getLogger().info(r.getName() + " valid rank loop");
                        List<NetworkUser> users = DAOManager.getNetworkUserDAO().createQuery().filter("rank =", r.getName()).asList();
                        List<NetworkUser> onlineUsers = new ArrayList<>();
                        for (NetworkUser u : users) {
                            Moderation.getInstance().getLogger().info(u.getName() + " online user loop");
                            if (IdentifierAPI.getPlayerServerID(u.getUuid()) > -1) {
                                Moderation.getInstance().getLogger().info(u.getName() + " online");
                                onlineUsers.add(u);
                            }
                        }
                        usersToSend.addAll(onlineUsers);
                    }

                    String message = "";
                    for (String s : args) {
                        message += s + " ";
                    }

                    for (NetworkUser u : usersToSend) {
                        Moderation.getInstance().getLogger().info("Message sent to " + u.getName());
                        Long playerID = IdentifierAPI.getPlayerServerID(u.getUuid());

                        Moderation.getInstance().getLogger().info("IDs: PlayerID" + playerID + ":serverID-" + serverID);

                        if (!playerID.equals(serverID)) {
                            MessageAPI.sendMessage(String.valueOf(IdentifierAPI.getPlayerServerID(u.getUuid())),
                                    Moderation.getPluginName(), false, "2:" + u.getUuid() + ":" + ((Player) sender).getUniqueId() + ":" + message);
                        } else {
                            Player p = Bukkit.getPlayer(UUID.fromString(u.getUuid()));
                            Rank r = DataAPI.getNetworkRank(u.getRank());
                            TextComponent component = new TextComponent("");
                            TextComponent staff = new TextComponent("[STAFF] ");
                            staff.setBold(true);
                            staff.setColor(ChatColor.RED);
                            staff.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/schat "));
                            TextComponent name = new TextComponent(FontFormat.translateString(r.getDisplayName() + u.getName() + " &6»&r "));
                            component.addExtra(staff);
                            component.addExtra(name);
                            component.addExtra(message);

                            p.spigot().sendMessage(component);
                        }
                    }
                    return true;
                } else {
                    sender.sendMessage(FontFormat.translateString("&cYou must include a message"));
                }
            }
        } else {
            sender.sendMessage(FontFormat.translateString("&cYou must be a player to execute this command!"));
        }
        return false;
    }
}
