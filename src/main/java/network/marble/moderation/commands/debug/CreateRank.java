package network.marble.moderation.commands.debug;

import network.marble.moderation.utils.FontFormat;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import network.marble.datastoragemanager.api.DataAPI;
import network.marble.datastoragemanager.database.models.Rank;
import network.marble.moderation.Moderation;
import network.marble.moderation.config.MainConfig;

public class CreateRank implements CommandExecutor {

	MainConfig config = Moderation.config;

	@SuppressWarnings("unused")
	public boolean onCommand(CommandSender sender, Command cmd, String s, String[] args) {
		if(Moderation.config.getDebug()){
			if(args.length > 0){
				if(args.length == 2){
					Rank check = DataAPI.getNetworkRank(args[0]);

					if(check != null){
						sender.sendMessage(ChatColor.RED + "Rank already exists!");
						return false;
					}

					Long start = System.currentTimeMillis();
					Rank rank = DataAPI.initRank(args[0], args[1]);
				
					sender.sendMessage(ChatColor.GREEN + "Rank " + ChatColor.GOLD + args[0]+ ChatColor.GREEN + " processed and added in " + (System.currentTimeMillis() - start) + "ms");
					return true;
				} else {
					sender.sendMessage(ChatColor.RED + "Incorrect Arguements, please refer to the usage");
					return false;
				}
			}
			else{
				sender.sendMessage(ChatColor.RED + "Too few arguements, please refer to the usage");
				return false;
			}
		}
		else{
			sender.sendMessage(FontFormat.translateString("&c" + cmd + " is not recognised as a command."));
			return false;
		}
	}
}
