package network.marble.moderation.commands.debug;

import network.marble.messageapi.api.MessageAPI;
import network.marble.moderation.permissions.Permissions;
import network.marble.serveridentifier.api.IdentifierAPI;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import net.md_5.bungee.api.ChatColor;
import network.marble.datastoragemanager.api.DataAPI;
import network.marble.datastoragemanager.database.DAOManager;
import network.marble.datastoragemanager.database.models.NetworkUser;
import network.marble.datastoragemanager.database.models.Rank;
import network.marble.moderation.Moderation;
import network.marble.moderation.config.MainConfig;
import network.marble.moderation.utils.FontFormat;
import org.bukkit.entity.Player;
import redis.clients.jedis.Jedis;

import java.util.UUID;

public class SetRank implements CommandExecutor {

	MainConfig config = Moderation.config;

	public boolean onCommand(CommandSender sender, Command cmd, String s, String[] args){
		if(Moderation.config.getDebug()){
			if (args.length != 0){
				if(args.length != 2){
					sender.sendMessage(ChatColor.RED + "Invalid parameters! \"/setrank [username] [rank]\"");
					return false;
				}
				else{
					Jedis jedis = null;
					String argUUID;
					try{
						jedis = Moderation.getPool().getResource();
						argUUID = jedis.get("playeruuid:" + args[0]);
					}
					catch(Exception e){
						sender.sendMessage(FontFormat.translateString("&cAn error occured whilst attempting to execute this command"));
						e.printStackTrace();
						return false;
					}

					if (argUUID == null){
					    argUUID = DataAPI.retrieveUserByName(args[0]).getUuid();
                    }
					
					NetworkUser u = DataAPI.retrieveUser(argUUID);
					Rank rank = DAOManager.getRankDAO().findOne("name", args[1]);

					if(rank == null && u == null){
						sender.sendMessage(ChatColor.RED + "Invalid user and Rank!");
						return false;
					}
					else if(rank == null){
						sender.sendMessage(ChatColor.RED + "Invalid Rank! " + ChatColor.GREEN + args[1] + ChatColor.RED + " does not exist");
						return false;
					}
					if(u == null){
						sender.sendMessage(ChatColor.RED + "Invalid Player name " + args[0]);
						return false;
					}
					else{
						Moderation.getInstance().getLogger().info(u.getName() + " " + u.getUuid() + " " + args[1]);
						u.setRank(args[1]);
						sender.sendMessage(ChatColor.GREEN + u.getName() + " set to " + args[1]);
						DAOManager.getNetworkUserDAO().save(u);


                        MessageAPI.sendMessage(String.valueOf(IdentifierAPI.getPlayerServerID(argUUID)), Moderation.getPluginName(), false, "1:" + argUUID);

						Moderation.getInstance().getLogger().info(u.getRank() + " " + u.getName());
						return true;
					}
				}
			}else{
				sender.sendMessage(ChatColor.RED + "Incorrect parameters! \"setrank [username] [rank]\"");
				return false;
			}
		}
		else{
			sender.sendMessage(FontFormat.translateString("&c" + cmd + " is not recognised as a command."));
			return false;
		}
	}
}
