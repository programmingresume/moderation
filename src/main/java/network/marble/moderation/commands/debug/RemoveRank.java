package network.marble.moderation.commands.debug;

import network.marble.moderation.utils.FontFormat;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.mongodb.morphia.query.Query;

import net.md_5.bungee.api.ChatColor;
import network.marble.datastoragemanager.database.DAOManager;
import network.marble.datastoragemanager.database.models.Rank;
import network.marble.moderation.Moderation;
import network.marble.moderation.config.MainConfig;

public class RemoveRank implements CommandExecutor {

	MainConfig config = Moderation.config;

	public boolean onCommand(CommandSender sender, Command cmd, String s, String[] args) {
		if(Moderation.config.getDebug()){
			if(args.length > 0){
				Rank rank = DAOManager.getRankDAO().findOne("name", args[0]);
				if(rank == null){
					sender.sendMessage(ChatColor.RED + "Rank does not exist");
				}
				else{
					Query<Rank> rankQuery = DAOManager.getDatastore().createQuery(Rank.class).filter("name", args[0]);
					DAOManager.getDatastore().delete(rankQuery);
					
					sender.sendMessage(ChatColor.GREEN + args[0] + " successfully removed");
				}
				return false;
			}else{
				sender.sendMessage(ChatColor.RED + "Incorrect parameter! Check usage");
				return false;
			}
		}else{
			sender.sendMessage(FontFormat.translateString("&c" + cmd + " is not recognised as a command."));
			return false;
		}
	}
}
