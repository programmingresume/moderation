package network.marble.moderation.commands.debug;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import net.md_5.bungee.api.ChatColor;
import network.marble.datastoragemanager.api.DataAPI;
import network.marble.datastoragemanager.database.DAOManager;
import network.marble.datastoragemanager.database.models.Rank;
import network.marble.moderation.Moderation;
import network.marble.moderation.config.MainConfig;
import network.marble.moderation.utils.FontFormat;

public class SetInheritance implements CommandExecutor {

	MainConfig config = Moderation.config;

	public boolean onCommand(CommandSender sender, Command cmd, String s, String[] args) {
		if(Moderation.config.getDebug()){
			if(args.length == 2){
				Rank rankInherits = DAOManager.getRankDAO().findOne("name", args[1]);
				Rank rank = DAOManager.getRankDAO().findOne("name", args[0]);

				if(rank == null || rankInherits == null){
					sender.sendMessage(ChatColor.RED + "One or more ranks do not exist!");
					return false;
				}
				else{
					rank.setInherits(rankInherits.getName());
					rank.save();
					sender.sendMessage(FontFormat.translateString("&aInheritance has been created between " + rank.getDisplayName() 
						+ rank.getName() + " &aand " + rankInherits.getDisplayName() + rankInherits.getName()));
					return true;
				}
			}
			else{
				sender.sendMessage(ChatColor.RED + "Invalid arguements! Check usage");
				return false;
			}
		}
		else{
			sender.sendMessage(FontFormat.translateString("&c" + cmd + " is not recognised as a command."));
			return false;
		}
	}

}
