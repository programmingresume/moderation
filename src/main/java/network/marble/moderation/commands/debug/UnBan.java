package network.marble.moderation.commands.debug;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import network.marble.datastoragemanager.api.DataAPI;
import network.marble.datastoragemanager.database.models.NetworkUser;
import network.marble.moderation.Moderation;
import network.marble.moderation.utils.FontFormat;
import redis.clients.jedis.Jedis;

public class UnBan implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if(Moderation.config.getDebug()){
			if (args.length == 1){
				Jedis jedis = null;
				String argUUID;
				
				try{
					jedis = Moderation.getPool().getResource();
					argUUID = jedis.get("playeruuid:" + args[0]);
				}
				catch(Exception e){
					sender.sendMessage(FontFormat.translateString("&cAn error occured whilst attempting to execute this command"));
					e.printStackTrace();
					return false;
				}
				
				NetworkUser user = DataAPI.retrieveUser(argUUID);

				if(user.getNetworkUserModeration().isBanned()){
					try{
						user.getNetworkUserModeration().setBanned(false);
						user.getNetworkUserModeration().setBanEndTime(0);
						DataAPI.saveUser(user);
						sender.sendMessage(FontFormat.translateString("&aUser " + args[0] + " has been successfully unbanned."));
						return true;
					} catch (Exception e){
						e.printStackTrace();
						sender.sendMessage(FontFormat.translateString("&cSomething appears to have gone wrong! Please notify and administrator."));
						return false;
					}
				} else {
					sender.sendMessage(FontFormat.translateString("&cThis person is not banned."));
					return false;
				}
			}
			else{
				sender.sendMessage(FontFormat.translateString("&c/unban <user>"));
				return false;
			}
		}
		else{
			sender.sendMessage(FontFormat.translateString("&c" + cmd + " is not recognised as a command."));
			return false;
		}
	}
}
