package network.marble.moderation.commands.debug;

import network.marble.moderation.utils.FontFormat;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.mongodb.morphia.query.Query;
import org.mongodb.morphia.query.UpdateOperations;

import com.mongodb.BasicDBObject;

import net.md_5.bungee.api.ChatColor;
import network.marble.datastoragemanager.api.DataAPI;
import network.marble.datastoragemanager.database.DAOManager;
import network.marble.datastoragemanager.database.models.Rank;
import network.marble.moderation.Moderation;
import network.marble.moderation.config.MainConfig;

public class RemovePermission implements CommandExecutor {

	MainConfig config = Moderation.config;

	public boolean onCommand(CommandSender sender, Command cmd, String s, String[] args) {
		if(Moderation.config.getDebug()){
			if (!(args.length > 2) || !(args.length < 2)){
				Rank rank = DataAPI.getNetworkRank(args[0]);
				if(rank == null){
					sender.sendMessage(ChatColor.RED + "Invalid Rank! " + ChatColor.GREEN + args[0] + ChatColor.RED + " does not exist");
					return false;
				}

				Long start = System.currentTimeMillis();

				Query<Rank> updateQuery = DAOManager.getDatastore().createQuery(Rank.class).disableValidation().field("name").equal(args[0]);
				UpdateOperations<Rank> ops = DAOManager.getDatastore().createUpdateOperations(Rank.class).removeAll("rankPermissions", new BasicDBObject("node", args[1]));
				DAOManager.getDatastore().update(updateQuery, ops);

				sender.sendMessage(ChatColor.GREEN + "Permission " + ChatColor.GOLD + args[1] 
						+ ChatColor.GREEN + " processed and removed in " + (System.currentTimeMillis() - start) + "ms");
				return true;
			}
			else{
				sender.sendMessage(ChatColor.RED + "Invalid perameters! Please check usage");
				return false;
			}
		}
		else{
			sender.sendMessage(FontFormat.translateString("&c" + cmd + " is not recognised as a command."));
			return false;
		}
	}

}
