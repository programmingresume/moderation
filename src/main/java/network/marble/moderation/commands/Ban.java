package network.marble.moderation.commands;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import network.marble.datastoragemanager.api.DataAPI;
import network.marble.datastoragemanager.database.models.NetworkUser;
import network.marble.moderation.Moderation;
import network.marble.moderation.utils.FontFormat;
import network.marble.moderation.utils.Time;
import redis.clients.jedis.Jedis;

public class Ban implements CommandExecutor {

	private String[] incorrectFormat = {"&c/ban <User> <[permanant]/[days]/[hours]/[minute]> [[days]/[hours]/[minute]..]",
	"&cFor example \"/ban <User> 1d 1h 1m\" or \"/ban <User> 1m\""};

	public long totalMillis = 0;

	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if(sender instanceof Player){
			if(sender.hasPermission("moderation.commands.ban")){
			//FIXME: Permissions
				if ((args.length >= 2)){
					for (int i = 1; i < args.length; i++){
						if (args[1].equalsIgnoreCase("permanant")){
							totalMillis = 157784630000L;
							break;
						}

						String time = args[i].replaceAll("[^A-Za-z]+", "");

						if (time.equalsIgnoreCase("d")){
							totalMillis += Time.DAY.getMillis() * getPeriod(args[i]);
						} else if (time.equalsIgnoreCase("h")){
							totalMillis += Time.HOUR.getMillis() * getPeriod(args[i]);
						} else if (time.equalsIgnoreCase("m")){
							totalMillis += Time.MINUTE.getMillis() * getPeriod(args[i]);
						} else {
							break;
						}
					}
					String argUUID = "";
					Jedis jedis = null;
					
					try{
						jedis = Moderation.getPool().getResource();
						argUUID = jedis.get("playeruuid:" + args[0]);
					} catch(Exception e) {
						sender.sendMessage(FontFormat.translateString("&cUnfortunately an error appears to have "
								+ "occured whilst communicating with the database. Please notify an Administrator or submit a bug report"));
						e.printStackTrace();
					}

					NetworkUser u = DataAPI.retrieveUser(argUUID);
                    NetworkUser user = DataAPI.retrieveUser(((Player) sender).getUniqueId().toString());

					if (u == null){
						sender.sendMessage(FontFormat.translateString(String.format("&cUser %d could not be found", args[0])));
						return false;
					}
					
					Player p = Bukkit.getPlayer(u.getName());
					Player pSender = Bukkit.getPlayer(sender.getName());

					if(p == null){
						pSender.sendMessage(FontFormat.translateString("&cPlayer &6" + args[0] + " &ccannot be found"));
					}

					if (totalMillis != 0){
						try {					
							if(jedis.exists("moderation:" + pSender.getUniqueId())){
								sender.sendMessage(FontFormat.translateString("&cYou must first confirm or "
										+ "cancel your previous ban attempt before attempting to ban somebody again"));
								return false;
							}
													
							Map<String, String> userData = new HashMap<String, String>();
							userData.put("targetUUID", p.getUniqueId().toString());
							userData.put("totalMillis", String.valueOf(totalMillis));
							
							jedis.hmset("moderation:" + pSender.getUniqueId(), userData);
						} catch (Exception e){
							pSender.sendMessage(FontFormat.translateString("&cUnfortunately an error appears to have "
									+ "occured whilst communicating with the database. Please notify an Administrator"));
							e.printStackTrace();
							return false;
						}

						TextComponent border = new TextComponent("----------------------------------------");
						border.setColor(ChatColor.GREEN);
						border.setBold(true);
						TextComponent message = new TextComponent("You are attempting to ban ");
						message.setColor(ChatColor.GREEN);
						message.setBold(true);
						TextComponent player = new TextComponent(args[0]);
						player.setColor(ChatColor.GOLD);
						player.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("Username: " 
								+ p.getDisplayName() + "\nUUID: " + p.getUniqueId() + "\nLast IP: " + u.getIp()).create()));
						message.addExtra(player);
						message.addExtra("! Are you sure?");
						TextComponent confirmCommand = new TextComponent("/confirm <reason>");
						confirmCommand.setColor(ChatColor.GOLD);
						confirmCommand.setBold(true);
						confirmCommand.setUnderlined(true);
						confirmCommand.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND , "/confirm "));
						TextComponent declineCommand = new TextComponent("/cancel");
						declineCommand.setColor(ChatColor.GOLD);
						declineCommand.setBold(true);
						declineCommand.setUnderlined(true);
						declineCommand.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND , "/cancel"));
						TextComponent finalString = new TextComponent("");
						finalString.setBold(true);
						finalString.setColor(ChatColor.GREEN);
						finalString.addExtra(confirmCommand);
						finalString.addExtra(" | ");
						finalString.addExtra(declineCommand);

						pSender.spigot().sendMessage(border);
						pSender.sendMessage("");
						pSender.spigot().sendMessage(message);
						pSender.sendMessage("");
						pSender.spigot().sendMessage(finalString);
						pSender.sendMessage("");
						pSender.spigot().sendMessage(border);

						return true;
					} else {
						sender.sendMessage(FontFormat.translateString("&cEither something has gone wrong or you attempted to "
								+ "ban somebody for 0 seconds. Please attempt the command again!"));
						return false;
					}
				}else{
					sender.sendMessage(FontFormat.translateString(incorrectFormat[0]));
					sender.sendMessage(FontFormat.translateString(incorrectFormat[1]));
					return false;
				}
			} else {
				sender.sendMessage(FontFormat.translateString("&cYou do not have permission to execute this command!"));
				return false;
			}
		} else {
			sender.sendMessage(FontFormat.translateString("&cYou must be a player to send this command! Please use the ban management interface instead."));
		}
		return false;
	}

	public long getPeriod(String s){
		String number = "";

		Pattern p = Pattern.compile("(\\d+)");
		Matcher m = p.matcher(s);
		while(m.find())
		{
			number += m.group(1);
		}

		return Long.parseLong(number);
	}
}
