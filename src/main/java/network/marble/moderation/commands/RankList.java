package network.marble.moderation.commands;

import java.util.List;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.mongodb.morphia.query.QueryResults;

import network.marble.datastoragemanager.api.DataAPI;
import network.marble.datastoragemanager.database.DAOManager;
import network.marble.datastoragemanager.database.models.NetworkUser;
import network.marble.datastoragemanager.database.models.Rank;
import network.marble.moderation.utils.FontFormat;

public class RankList implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (sender instanceof Player){
			NetworkUser u = DataAPI.retrieveUser(((Player) sender).getUniqueId().toString());
			if (sender.hasPermission("moderation.commands.ranklist")){
				QueryResults<Rank> ranks = DAOManager.getRankDAO().find();
				
				sender.sendMessage(FontFormat.translateString("List of all ranks"));
				for (Rank r : ranks){
					sender.sendMessage(FontFormat.translateString(r.getDisplayName() + r.getName()));
				}
			}
		}else{
			sender.sendMessage(FontFormat.translateString("&cYou must be a Player to send this command!"));
		}
		return false;
	}
}
