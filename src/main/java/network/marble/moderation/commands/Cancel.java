package network.marble.moderation.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import network.marble.datastoragemanager.api.DataAPI;
import network.marble.datastoragemanager.database.models.NetworkUser;
import network.marble.moderation.Moderation;
import network.marble.moderation.utils.FontFormat;
import redis.clients.jedis.Jedis;

public class Cancel implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (sender instanceof Player) {
			NetworkUser user = DataAPI.retrieveUser(((Player) sender).getUniqueId().toString());
			if (sender.hasPermission("moderation.commands.ban")) {
				//FIXME: Permissions
				Player p = Bukkit.getPlayer(sender.getName());
				Jedis jedis = null;
				try {
					jedis = Moderation.getPool().getResource();

					if (jedis.exists("moderation:" + p.getUniqueId())) {
						jedis.del("moderation:" + p.getUniqueId());
						p.sendMessage(FontFormat.translateString("&aBan has successfully been canceled."));
						return true;
					} else {
						p.sendMessage(FontFormat.translateString("&cYou must have a pending ban in order to cancel one."));
					}
				} catch (Exception e) {
					p.sendMessage(FontFormat.translateString("&cThere was an error whilst communicating with the database. "
							+ "Please try again shortly or create a bug report."));
					e.printStackTrace();
					return false;
				}
			} else {
				sender.sendMessage(FontFormat.translateString("&cYou do not have permission to execute this command!"));
				return false;
			}
		} else {
			sender.sendMessage(FontFormat.translateString("&cYou must be a player in order to send this command"));
		}
		return false;
	}
}
