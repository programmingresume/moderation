package network.marble.moderation.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import network.marble.datastoragemanager.api.DataAPI;
import network.marble.datastoragemanager.database.DAOManager;
import network.marble.datastoragemanager.database.models.ModerationLog;
import network.marble.datastoragemanager.database.models.NetworkUser;
import network.marble.moderation.utils.FontFormat;

public class Kick implements CommandExecutor {

	@SuppressWarnings("unused")
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (sender instanceof Player){
			NetworkUser user = DataAPI.getUser(((Player) sender).getUniqueId().toString());
			if(sender.hasPermission("moderation.commands.kick")){
				if (args.length >= 2){
					NetworkUser player = DataAPI.retrieveUserByName(args[0]);
					Player pSender = (Player) sender;
					Player p = Bukkit.getPlayer(player.getUuid());

					if (player == null){
						sender.sendMessage(FontFormat.translateString("&cThis player does not exist"));
						return false;
					}

					String reason = "";
					for(int i = 1; i < args.length; i++){
						if (!(args[i] == null)){
							reason += " " + args[i];
						}
					}

					if (reason == "")
						sender.sendMessage(FontFormat.translateString("&cYou must provide a reason! /kick <user> <reason...>"));

					ModerationLog m = new ModerationLog(pSender.getUniqueId().toString(), p.getUniqueId().toString(), System.currentTimeMillis(), "KICK", reason, 0L);
					DAOManager.getModerationLogDAO().save(m);

					p.kickPlayer("You were kicked for reason: " + reason);
					sender.sendMessage(FontFormat.translateString("&aPlayer " + args[0] + " has been kicked"));
					return true;
				} else {
					sender.sendMessage(FontFormat.translateString("&c/kick <user> <reason...>"));
				}
			} else {
				sender.sendMessage(FontFormat.translateString("&cYou do not have permission to execute this command!"));
				return false;
			}
		} else {
			sender.sendMessage(FontFormat.translateString("&cYou must be a player in order to send this command!"));
		}
		return false;
	}
}
