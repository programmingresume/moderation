package network.marble.moderation.commands;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import network.marble.datastoragemanager.api.DataAPI;
import network.marble.datastoragemanager.database.DAOManager;
import network.marble.datastoragemanager.database.models.ModerationLog;
import network.marble.datastoragemanager.database.models.NetworkUser;
import network.marble.moderation.utils.FontFormat;
import network.marble.moderation.utils.Time;

public class Mute implements CommandExecutor {

	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (sender instanceof Player){
			NetworkUser user = DataAPI.getUser(((Player) sender).getUniqueId().toString());
			if (sender.hasPermission("moderation.commands.mute")){
				if (args.length > 2){
					Long totalMillis = 0L;
					int length = 0;
					
					NetworkUser u = DataAPI.retrieveUserByName(args[0]);
					if (u == null){
						sender.sendMessage(FontFormat.translateString("&cUser " + args[0] + "could not be found"));
						return false;
					}
					
					for (int i = 1; i < args.length; i++){
						if (args[1].equalsIgnoreCase("permanant")){
							totalMillis = 157784630000L;
							break;
						}

						String time = args[i].replaceAll("[^A-Za-z]+", "");

						if (time.equalsIgnoreCase("d")){
							totalMillis += Time.DAY.getMillis() * getPeriod(args[i]);
						} else if (time.equalsIgnoreCase("h")){
							totalMillis += Time.HOUR.getMillis() * getPeriod(args[i]);
						} else if (time.equalsIgnoreCase("m")){
							totalMillis += Time.MINUTE.getMillis() * getPeriod(args[i]);
						} else {
							length = i;
							break;
						}
					}
					
					String reason = "";
					for (int i = length; i < args.length; i++){
						reason += args[i];
					}
					
					ModerationLog m = new ModerationLog(user.getUuid(), u.getUuid(), System.currentTimeMillis(), "MUTE", reason, totalMillis + System.currentTimeMillis());
					DAOManager.getModerationLogDAO().save(m);
					
					u.getNetworkUserModeration().setMuteEndTime(totalMillis + System.currentTimeMillis());
					
					sender.sendMessage(FontFormat.translateString("&aUser " + args[0] + " has been muted"));
				} else {
					sender.sendMessage(FontFormat.translateString("Incorrect perameters! /mute (user) (amount of time) (reason...)"));
					return false;
				}
			} else {
				sender.sendMessage(FontFormat.translateString("&cYou do not have permission to execute this command!"));
			}
		} else {
			sender.sendMessage(FontFormat.translateString("&cYou must be a player to send this command"));
		}
		return false;
	}
	
	public long getPeriod(String s){
		String number = "";

		Pattern p = Pattern.compile("(\\d+)");
		Matcher m = p.matcher(s);
		while(m.find())
		{
			number += m.group(1);
		}

		return Long.parseLong(number);
	}
}
