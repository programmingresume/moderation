package network.marble.moderation.listeners;

import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import network.marble.datastoragemanager.database.DAOManager;
import network.marble.moderation.permissions.Permissions;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent.Result;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.help.HelpTopic;

import net.md_5.bungee.api.ChatColor;
import network.marble.datastoragemanager.api.DataAPI;
import network.marble.datastoragemanager.database.models.NetworkUser;
import network.marble.datastoragemanager.database.models.Rank;
import network.marble.moderation.Moderation;
import network.marble.moderation.utils.FontFormat;

public class PlayerListener implements Listener {

    String template = null;

    @EventHandler
    public void onPlayerPreLogin(AsyncPlayerPreLoginEvent e) {
        NetworkUser user = DataAPI.getUser(e.getUniqueId().toString());
        Moderation.getInstance().getLogger().info(user.getName());
        long now = System.currentTimeMillis();
        if (user.getNetworkUserModeration().isBanned()) {
            Moderation.getInstance().getLogger().info(e.getName() + " banned");
            if (user.getNetworkUserModeration().getBanEndTime() > now) {
                long diff = user.getNetworkUserModeration().getBanEndTime() - now;

                Calendar calendar = Calendar.getInstance();
                calendar.setTimeInMillis(user.getNetworkUserModeration().getBanEndTime());

                long days = TimeUnit.MILLISECONDS.toDays(diff);
                diff -= TimeUnit.DAYS.toMillis(days);
                long hours = TimeUnit.MILLISECONDS.toHours(diff);
                diff -= TimeUnit.HOURS.toMillis(hours);
                long minutes = TimeUnit.MILLISECONDS.toMinutes(diff);

                e.disallow(Result.KICK_BANNED, "You will be able to rejoin on the "
                        + calendar.get(Calendar.DAY_OF_MONTH)
                        + "-"
                        + calendar.get(Calendar.MONTH)
                        + "-"
                        + calendar.get(Calendar.YEAR)
                        + ". This is in "
                        + String.format("%d day(s), %d hours and %d minutes",
                        days, hours, minutes));
            } else {
                user.getNetworkUserModeration().setBanned(false);
                user.getNetworkUserModeration().setBanEndTime(0);
                user.getNetworkUserModeration().setBanReason("");
                DataAPI.saveUser(user);
            }
        }
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e) {
        Player p = e.getPlayer();
        Permissions.addPlayerAttachment(p);
    }

    @EventHandler
    public void onPlayerChat(AsyncPlayerChatEvent e) {
        Moderation.getInstance().getLogger().info("onPlayerChat Called");

        Player p = e.getPlayer();
        NetworkUser user = DataAPI.retrieveUser(p.getUniqueId().toString());
        Rank rank = DataAPI.getNetworkRank(user.getRank());

        if(rank == null){
            user.setRank("DEFAULT");
            DAOManager.getNetworkUserDAO().save(user);
            rank = DataAPI.getNetworkRank(user.getRank());
        }

        String message = e.getMessage().replaceAll("%", "%%");

        if (template == null) {
            template = "[prefix][name] &6»&r[suffix] ";
        }

        long muteTime = user.getNetworkUserModeration().getMuteEndTime();
        if (muteTime < System.currentTimeMillis()) {
            user.getNetworkUserModeration().setMuted(false);
            DataAPI.saveUser(user);
        }
        if (!user.getNetworkUserModeration().isMuted()) {
            String formattedName = template.replaceAll("\\[prefix\\]", rank.getDisplayName()).replaceAll("\\[name\\]",
                    p.getDisplayName()).replaceAll("\\s+", " ").replaceAll("\\[suffix\\]", "&r"); //TODO: Perhaps implement suffixes

            if (p.hasPermission("moderation.chat.color")) {
                e.setFormat(FontFormat.translateString(formattedName + message));
            } else {
                e.setFormat(FontFormat.translateString(formattedName) + message);
            }
        } else {
            e.setCancelled(true);
            p.sendMessage(ChatColor.RED + "You are Muted!");
        }
    }

    @EventHandler
    public void onPlayerCommand(PlayerCommandPreprocessEvent e) {
        Player player = e.getPlayer();
        String cmd = e.getMessage().split(" ")[0];
        String unknown = "&c" + cmd + " is not recognised as a command.";

        if (cmd.equals("/help")) {
            player.sendMessage(FontFormat.translateString(unknown));
            e.setCancelled(true);
        } else if (cmd.equals("/pl")) {
            player.sendMessage(FontFormat.translateString(unknown));
            e.setCancelled(true);
        } else if (cmd.equals("/?")) {
            player.sendMessage(FontFormat.translateString(unknown));
            e.setCancelled(true);
        } else if (cmd.equals("/plugins")) {
            player.sendMessage(FontFormat.translateString(unknown));
            e.setCancelled(true);
        }

        if (!e.isCancelled()) {
            HelpTopic topic = Bukkit.getServer().getHelpMap().getHelpTopic(cmd);
            if (topic == null) {
                player.sendMessage(FontFormat.translateString(unknown));
                e.setCancelled(true);
            }
        }
    }
}
