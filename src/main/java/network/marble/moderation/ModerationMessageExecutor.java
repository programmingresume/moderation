package network.marble.moderation;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.TextComponent;
import network.marble.datastoragemanager.api.DataAPI;
import network.marble.datastoragemanager.database.models.NetworkUser;
import network.marble.datastoragemanager.database.models.Rank;
import network.marble.messageapi.api.MessageExecutor;
import network.marble.moderation.permissions.Permissions;
import network.marble.moderation.utils.FontFormat;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.UUID;

public class ModerationMessageExecutor implements MessageExecutor {

    @Override
    public void executeMessage(String s) {
        if(s.startsWith("1:")){
            String uuid = s.substring(2);
            Player p = Bukkit.getPlayer(UUID.fromString(uuid));
            if(p != null){
                Permissions.recalculatePermissions(p);
            }
        }
        if(s.startsWith("2:")){
            String[] args = s.split(":");
            Player addressee = Bukkit.getPlayer(UUID.fromString(args[1]));
            NetworkUser u = DataAPI.retrieveUser(args[2]);
            Rank r = DataAPI.getNetworkRank(u.getRank());
            if(addressee != null){
                TextComponent message = new TextComponent("");
                TextComponent staff = new TextComponent("[STAFF] ");
                staff.setBold(true);
                staff.setColor(ChatColor.RED);
                staff.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/schat "));
                TextComponent name = new TextComponent(FontFormat.translateString(r.getDisplayName() + u.getName() + " &6»&r "));
                message.addExtra(staff);
                message.addExtra(name);
                message.addExtra(args[3]);

                addressee.spigot().sendMessage(message);
            }
        }
    }
}
